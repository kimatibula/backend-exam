<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_api extends MY_controller {
     public function __construct()
    {
        parent::__construct();
       
       
    }

    public function index(){
        $params = json_decode(file_get_contents('php://input'), TRUE);
        $title                  =   $params['title'];
        $content                =   $params['content'];
        $image                  =   $params['image'];
        $validate               =   $this->MY_Model->validate_post($content,$title);
        if($validate['status'] == 0){
            $insert_post	=	$this->MY_Model->insert('posts',array(
                    'user_id' => $this->session->userdata('user_id'),
                    'title' => $title, 
                    'content' =>$content, 
                    'slug'  => $title,
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s"),
                    'image' =>  $image
                   ));
            if($insert_post > 0){
                $get_posts = $this->MY_Model->select('title,content,slug,created_at,updated_at,id,user_id','posts',array('id' =>$insert_post));
                json_output(201,$this->status_code(201,'Created',$get_posts));
            }
            else{
                json_output(203,$this->status_code(203,'Error to create posts',''));
            }
        }
        

    }

    public function patch_post($slug){
        $params = json_decode(file_get_contents('php://input'), TRUE);
        $title  = $params['title'];

        $check_if_exists = $this->MY_Model->check_if_exists('*','posts',array('slug' =>$slug));
        if ($check_if_exists->num_rows() > 0 ) 
        {
            
            $result         = $check_if_exists->result();
            $post_id      = $result[0]->id; 

            $update = $this->MY_Model->update('posts',array('slug' => $slug),array('title'=>$title,'slug'=>$title,'updated_at'=>date("Y-m-d H:i:s")));
            if($update){
                $get_post = $this->MY_Model->select('title,content,slug,created_at,updated_at,id,user_id,deleted_at','posts',array('id' =>$post_id ));
                json_output(200, $this->status_code(200,'ok',$get_post));
            }
        }
        else{
            json_output(404, $this->status_code(404,'No query results for model [App\\Post].',''));
        }

    }

    public function delete_post($slug){
        $check_if_exists = $this->MY_Model->check_if_exists('*','posts',array('slug' =>$slug));
        if ($check_if_exists->num_rows() > 0 ) 
        {
            $delete = $this->MY_Model->delete('posts',array('slug'=>$slug));
            if($delete){
                json_output(200, array( "status"=> "record deleted successfully"));
            }
            else{
                json_output(203, array( "status"=> "Error to delete"));
            }

        }
        else{
            json_output(404, $this->status_code(404,'No query results for model [App\\Post].',''));
        }
    }

    public function create_comment($slug){
        $params = json_decode(file_get_contents('php://input'), TRUE);
        $body                  =   $params['body'];
        $parent_id             =   $params['parent_id'];
        $check_if_exists = $this->MY_Model->check_if_exists('*','posts',array('slug' =>$slug));
        if ($check_if_exists->num_rows() > 0 ) 
        {
            $validate = $this->MY_Model->validate_comment($body,$parent_id);
            if($validate['status'] == 200){
                $insert_comment	=	$this->MY_Model->insert('comment',array(
                    'creator_id' => $this->session->userdata('user_id'),
                    'body' => $body, 
                    'commentable_type' =>"App\\Post", 
                    'parent_id'  => $parent_id,
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s")
                   ));
                if($insert_comment > 0){
                    $get_comment = $this->MY_Model->select('creator_id,body,parent_id,commentable_type,created_at,updated_at','comment',array('id' =>$insert_comment));
                    json_output(201,$this->status_code(201,'Created',$get_comment));
                }
                
            } 
        }
        else{
            json_output(404, $this->status_code(404,'No query results for model [App\\Post].',''));
        }
       
    }


}

