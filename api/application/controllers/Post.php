<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post extends MY_controller{

    public function __construct()
    {
        parent::__construct();
        $this->output->set_content_type('application/json');
    }

    public function index(){
        $get_all_post = $this->MY_Model->select('*','posts');
        json_output(200, $this->status_code(200,'ok',$get_all_post));
    }

    public function post_new_title($slug = ''){
        $check_if_exists = $this->MY_Model->check_if_exists('*','posts',array('slug' =>$slug));
        if ($check_if_exists->num_rows() > 0 ) 
        {
            $get_post = $this->MY_Model->select('*','posts',array('slug' =>$slug));
            json_output(200, $this->status_code(200,'ok',$get_post));
        }
        else{
            json_output(404, $this->status_code(404,'No query results for model [App\\Post].',''));
        }
    }

    public function comment($slug = ''){
        $check_if_exists = $this->MY_Model->check_if_exists('*','posts',array('slug' =>$slug));
        if ($check_if_exists->num_rows() > 0 ) 
        {
            $result         = $check_if_exists->result();
            $post_id       = $result[0]->id;
            $get_comment    = $this->MY_Model->select('*','comment',array('commentable_id' =>$post_id));
            json_output(200, $this->status_code(200,'ok',$get_comment));
        }
        else{
            json_output(404, $this->status_code(404,'No query results for model [App\\Post].',''));
        }
    }
}