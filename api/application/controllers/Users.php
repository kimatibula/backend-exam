<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MY_controller{

    public function __construct()
    {
        parent::__construct();
        $this->output->set_content_type('application/json');
    }

    public function create_user(){
        $params = json_decode(file_get_contents('php://input'), TRUE);
        $name                   =   $params['name'];
        $email                  =   $params['email'];
        $password               =   $params['password'];
        $password_confirmation  =   $params['password_confirmation'];
        $validate = $this->MY_Model->validate_register($name,$email,$password,$password_confirmation,array('email'=>$email));
        if($validate['status'] == 200){
            $insert_user	=	$this->MY_Model->insert('users',array('name' => $name,'email' => $email, 'password' =>$password, 'created_at' => date("Y-m-d H:i:s"),'updated_at' => date("Y-m-d H:i:s"),));
            if($insert_user > 0){
                $get_user = $this->MY_Model->select('name,email,created_at,updated_at,id,','users',array('id' =>$insert_user));
                json_output(201,$this->status_code(201,'Created',$get_user));
            }
            else{
                json_output(203,$this->status_code(203,'Error to register',''));
            }
        }
    }

    public function login(){
        $params = json_decode(file_get_contents('php://input'), TRUE);
        $email                  =   $params['email'];
        $password               =   $params['password'];
        $validate = $this->MY_Model->validate_login($email,$password,array('email'=>$email), array('password'=>$password));
        if($validate['status'] == 200){
            // echo 'sss';
            $this->session->set_userdata('user_id', $validate['data'][0]->id);
            json_output(200, $this->MY_Model->status_code_login($this->generateDataToken($validate['data']),date("Y-m-d H:i:s", strtotime('+12 hours'))));
        }
    }

}