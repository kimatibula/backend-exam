<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// $route['default_controller'] = 'pages/view';
$route['default_controller']        =   'post/index';

// MAIN PAGES

$route['posts']['get']                     =   'post/index';
$route['posts/(:any)']['get']             =    'post/post_new_title/$1';
$route['posts/(:any)/comments']['get']     =   'post/comment/$1';

$route['register']['post']                  =   'users/create_user';
$route['login']['post']                     =   'users/login';
$route['posts']['post']                     =   'auth_api';
$route['posts/(:any)']['patch']             =   'auth_api/patch_post/$1';
$route['posts/(:any)']['delete']            =   'auth_api/delete_post/$1';

$route['posts/(:any)/comments']['post']     =   'auth_api/create_comment/$1';

$route['404_override'] = '';
$route['translate_uri_dashes']      =   FALSE;
$route['assets/(:any)']             =   'assets/$1';

