<?php

require APPPATH . '/libraries/JWT.php';


class ImplementJwt
{
   

    //Generate Token
    PRIVATE $key = "geek-doctor-api-key";
    public function GenerateToken($data)
    {          
        $jwt = JWT::encode($data, $this->key);
        return $jwt;
    }
    


   //Decode Token 
    public function DecodeToken($token)
    {          
        $decoded = JWT::decode($token, $this->key, array('HS256'));
        $decodedData = (array) $decoded;
        return $decodedData;
    }
}
?> 