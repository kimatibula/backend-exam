<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH. '/libraries/ImplementJwt.php';
Class MY_controller extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->load->library('pagination');
		$this->load->model('MY_Model');
		$this->objOfJwt = new ImplementJwt();
	}

	// GENERATE TOKEN IN API
	public function generateDataToken($tokenData){
		$jwtToken	=	$this->objOfJwt->GenerateToken($tokenData);
		return $jwtToken;
	}

	// DECODE TOKEN IN UI
	public function decodeDataToken($token){
		try
		{
			$jwtData = $this->objOfJwt->DecodeToken($token);
			return json_encode($jwtData);
		}
		catch(Exception $e){
			// return json_encode($this->status_code('404',$e->getMessage(),''));exit;
			return false;
		}

	}

	

	public function status_code($status,$msg,$data){
		return array(
			'status' 	=> 	$status,
			'message'	=>	$msg,
			'data'		=>	$data
		);
	}


	 public function logout(){
		$this->session->unset_userdata('user_dtls');
		redirect('login');
	}
}
