<?php
defined('BASEPATH') OR exit('No direct script access allowed');

Class MY_Model extends CI_Model{
    public function __construct(){
      parent::__construct();
    }

    

    public function login($select = '*',$table,$and_where = array()){
      $check_user       = $this->check_if_exists($select,$table,$and_where);
      $response          = array();
      if ($check_user->num_rows() > 0 ) 
      {
          $user         = $check_user->result();
          $user_id      = $user[0]->id; 
          $status       = $user[0]->status_id;
          if($status == 7){
              $response    =  $this->status_code(406,'Invalid request! Please contact the administrator.', '');
          }
          else if($status == 1){
              $response    =  $this->status_code(406,'Please verify your account', '');
          }
          else{
              $date         = date('Y-m-d H:i:s');
              $token        = md5(rand());
              $role         = $this->check_if_exists('role_id','users',array('id' => $user_id));
              $role_type    = $role->result();
              $expired_at   = date("Y-m-d H:i:s", strtotime('+12 hours'));
              $update_user  = $this->update('users',array('id' => $user_id),array('last_login' => $date));
              if($update_user){
                $response    = $this->status_code(200, 'Successfully login.',$this->user_data($user_id,$expired_at,$role_type[0]->role_id));
              }
              else
              {
                $response   = $this->status_code(403,'Error to update last login', '');
              }
          }
          
      }
      else{
        $response   = $this->status_code(203,'User not found'.$and_where['username'],  '');
      }

      return $response;


  }


  public function api_save($table, $where = array(), $data_insert = array()){
      $check_if_exists        =   $this->check_if_exists('*',$table,$where);
      if($check_if_exists->num_rows() > 0){
          json_output(409, $this->status_code(409,'Already exists!',''));
      }
      else{
          $insert_data    =   $this->insert($table,$data_insert);
          if($insert_data > 0){
              json_output(200, $this->status_code(200,'Successfully created!',''));
          }
          else{
              json_output(203, $this->status_code(203,'Error to insert'));
          }
      }
  }

  public function api_update($table, $where = array(), $id = array(), $set = array()){

    $check_if_exists    =   $this->MY_Model->check_if_exists('*',$table,$where);
    if($check_if_exists->num_rows() > 0){
        json_output(409, $this->status_code(409,'Already Exists!',''));
    }
    else{
        $update_data=   $this->MY_Model->update($table,$id, $set);
        if($update_data){
            json_output(200, $this->status_code(200,'Successfully updated',''));
        } 
        else{
            json_output(203, $this->status_code(203,'Error to update',''));
        }
    }
  }

  public function status_code($status,$msg,$data){
		return array(
			'status' 	=> 	$status,
			'message'	=>	$msg,
			'data'		=>	$data
		);
  }

  public function status_code_login($token,$expires_at){
    return array(
      'token' 	=> 	$token,
			'token_type'	=>	"bearer",
			'expires_at'		=>	$expires_at
    );
  }
  
  public function status_code_error($status,$msg,$data){
    return array(
			'status' 	=> 	$status,
			'message'	=>	$msg,
			'error'		=>	$data
		);
  }

    

    public function user_data($id,$expire_at,$user_type){
      return array(
        'user_id'       => $id,
        'expire_at'        => $expire_at,
        'user_type'     => $user_type
      );
    }

    public function check_if_exists($select = '*', $table, $and_where = array()){
        $this->db->select($select);
        $this->db->from($table);
        $this->db->where($and_where);
        $this->db->limit(1);
        $query = $this->db->get();
        return $query;
    }
    public function validate_comment($body,$and_id){
      $error = array();
      if($body == ""){
        $error['body'] = 'The body field is required.';
      }
      if(empty($error)){
        
          return $this->status_code(200,'Valid.',$query->result());
        
      }
      else{
        json_output(422,$this->status_code_error(422,'The The given data was invalid.',$error));
      }
      
    }

    public function validate_post($content,$title){
      $error = array();
      if($content == ""){
        $error['content'] = 'The content field is required.';
      }
      if($title == ""){
        $error['title'] = "The title field is required.";
      }

      if($content == "" && $title == ""){
        $error['content'] = 'The content field is required.';
        $error['title'] = "The title field is required.";
      }
      if(empty($error)){
        return $this->status_code(200,'Valid.',$query->result());
      }
      else{
        json_output(422,$this->status_code_error(422,'The The given data was invalid.',$error));
      }
    }

    public function validate_login($email,$password,$and_email, $and_pass){
      $error = array();
      if($email == ""){
        $error['email'] = 'The email field is required.';
      }
      if($password == ""){
        $error['password'] = "The password field is required.";
      }

      if($email == "" && $password == ""){
        $error['email'] = 'The email field is required.';
        $error['password'] = "The password field is required.";
      }
      if(empty($error)){
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where($and_email);
        $this->db->limit(1);
        $email = $this->db->get();
        if($email->num_rows() > 0){
              $this->db->select('*');
              $this->db->from('users');
              $this->db->where($and_pass);
              $this->db->limit(1);
              $query = $this->db->get();
              if ($query->num_rows() > 0 ) 
              {
                return $this->status_code(200,'Logined.',$query->result());
                // json_output(200,$this->status_code_login($token,$expires_at))
              }
              else{
                $error['password'] = 'These credentials do not match our records.';
                json_output(422,$this->status_code_error(422,'The The given data was invalid.',$error));
              }
        }
        else{
          $error['email'] = "These credentials do not match our records.";
          json_output(422,$this->status_code_error(422,'The The given data was invalid.',$error));
        }
         
    }
    else{
      json_output(422,$this->status_code_error(422,'The The given data was invalid.',$error));
    }

      
    }

    public function validate_register($name,$email,$pass,$c_pass,$and_where = array()){
      $error = array();
      if($name == ""){
        $error['name'] = "The name field is required.";
      }

      if($email == ""){
        $error['email'] = "The email field is required.";
      }

      if($pass == "" || $c_pass == ""){
        $error['password'] = "The password field is required.";
      }

      if($pass != $c_pass){
        $error['password'] = "The password confirmation does not match.";
      }

      $this->db->select('*');
      $this->db->from('users');
      $this->db->where($and_where);
      $this->db->limit(1);
      $query = $this->db->get();
      if ($query->num_rows() > 0 ) 
      {
        $error['email'] = "The email has already been taken.";
        json_output(422,$this->status_code_error(422,'The The given data was invalid.',$error));
      }
      else{
        if(empty($error)){
          return $this->status_code(200,'Successfully Registered.','');
        }
        else{
          json_output(422,$this->status_code_error(422,'The given data was invalid.',$error));
        }
      }

    }

    
    // INSERT
    public function insert($table,$data = array()){
      $this->db->insert($table,$data);
      if ( $this->db->affected_rows() > 0 ) {
        return $this->db->insert_id();
      }
      else{
        return false;
      }
    }

     // UPDATE
     public function update($table,$id = array(),$set = array()){
      $this->db->trans_start();
      $this->db->set($set);
      $this->db->where($id);
      $this->db->update($table);
      $this->db->trans_complete();
      if ($this->db->trans_status() === FALSE)
      {
        return false;
      }
      else{
        return true;
      }
    }

    // DELETE
    public function delete($table,$id = array())
    {
      $this->db->delete($table,$id);
      if ($this->db->affected_rows() > 0) {
        return true;
      }
      else{
        return false;
      }
    }


    // SELECT
    public function select($select = '*',$table, $and_where = array(),$or_where = array(),$join = array(),$join_type = 'LEFT',$order = array(), $limit = array(),$like = array(),$or_like = array() , $group_by = ''){
      $this->db->select($select);
      $this->db->from($table);
      if (!empty($and_where)) {
        $this->db->where($and_where);
      }
      if (!empty($or_where)) {
        $this->db->where($or_where);
      }
      if (!empty($join)) {
        if (!empty($join_type)) {
          foreach ($join as $key => $value) {
            $this->db->join($key,$value,$join_type);
          }
        }
        else{
          foreach ($join as $key => $value) {
            $this->db->join($key,$value);
          }
        }
      }
      if(!empty($order)){
        foreach ($order as $key => $value){
          $this->db->order_by($key, $value);
        }
      }
      if(!empty($limit)){
         $this->db->limit($limit[0],$limit[1]);
        // foreach ($limit as $key => $value) {
        //   $this->db->limit($key,$value);
        // }
      }

      if(!empty($like)){
        foreach ($like as $key => $value){
          $this->db->like($key, $value);
        }
      }
      if(!empty($or_like)){
        $this->db->group_start();
        foreach ($or_like as $key => $value){
          $this->db->or_like($key, $value);
        }
        $this->db->group_end();
      }

      if($group_by != ''){
        $this->db->group_by($group_by); 
      }
      return $this->db->get()->result();
    }

    public function tableServerSide($select = '*',$table, $and_where = array(),$or_where = array(),$join = array(),$join_type = 'LEFT',$order = array(), $limit = array(),$like = array(),$or_like = array()){
      $this->db->select($select);
      $this->db->from($table);
      if (!empty($and_where)) {
        $this->db->where($and_where);
      }
      if (!empty($or_where)) {
        $this->db->where($or_where);
      }
      if (!empty($join)) {
        if (!empty($join_type)) {
          foreach ($join as $key => $value) {
            $this->db->join($key,$value,$join_type);
          }
        }
        else{
          foreach ($join as $key => $value) {
            $this->db->join($key,$value);
          }
        }
      }
      if(!empty($order)){
        foreach ($order as $key => $value){
          $this->db->order_by($key, $value);
        }
      }
      if(!empty($limit)){
         // $this->db->limit($limit[0],$limit[1]);
        foreach ($limit as $key => $value) {
          $this->db->limit($key,$value);
        }
      }

      if(!empty($like)){
        foreach ($like as $key => $value){
          $this->db->like($key, $value);
        }
      }
      if(!empty($or_like)){
        $this->db->group_start();
        foreach ($or_like as $key => $value){
          $this->db->or_like($key, $value);
        }
        $this->db->group_end();
      }
      return $this->db->get()->result();
    }
    public function getRows($select = '*',$table, $and_where = array(),$or_where = array(),$join = array(),$join_type = 'LEFT',$order = array(), $limit = array(),$like = array(),$or_like = array()){
      $this->db->select($select);
      $this->db->from($table);
      if (!empty($and_where)) {
        $this->db->where($and_where);
      }
      if (!empty($or_where)) {
        $this->db->where($or_where);
      }
      if (!empty($join)) {
        if (!empty($join_type)) {
          foreach ($join as $key => $value) {
            $this->db->join($key,$value,$join_type);
          }
        }
        else{
          foreach ($join as $key => $value) {
            $this->db->join($key,$value);
          }
        }
      }
      if(!empty($order)){
        foreach ($order as $key => $value){
          $this->db->order_by($key, $value);
        }
      }
      if(!empty($limit)){
         $this->db->limit($limit[0],$limit[1]);
        // foreach ($limit as $key => $value) {
        //   $this->db->limit($key,$value);
        // }
      }

      if(!empty($like)){
        foreach ($like as $key => $value){
          $this->db->like($key, $value);
        }
      }
      if(!empty($or_like)){
        $this->db->group_start();
        foreach ($or_like as $key => $value){
          $this->db->or_like($key, $value);
        }
        $this->db->group_end();
      }
      return $this->db->get()->result();
    }

    
    function timeAgo($time_ago)
    {
        $time_ago = strtotime($time_ago);
        $cur_time   = time();
        $time_elapsed   = $cur_time - $time_ago;
        $seconds    = $time_elapsed ;
        $minutes    = round($time_elapsed / 60 );
        $hours      = round($time_elapsed / 3600);
        $days       = round($time_elapsed / 86400 );
        $weeks      = round($time_elapsed / 604800);
        $months     = round($time_elapsed / 2600640 );
        $years      = round($time_elapsed / 31207680 );
        // Seconds
        if($seconds <= 60){
            return "just now";
        }
        //Minutes
        else if($minutes <=60){
            if($minutes==1){
                return "one minute ago";
            }
            else{
                return "$minutes minutes ago";
            }
        }
        //Hours
        else if($hours <=24){
            if($hours==1){
                return "an hour ago";
            }else{
                return "$hours hrs ago";
            }
        }
        //Days
        else if($days <= 7){
            if($days==1){
                return "yesterday";
            }else{
                return "$days days ago";
            }
        }
        //Weeks
        else if($weeks <= 4.3){
            if($weeks==1){
                return "a week ago";
            }else{
                return "$weeks weeks ago";
            }
        }
        //Months
        else if($months <=12){
            if($months==1){
                return "a month ago";
            }else{
                return "$months months ago";
            }
        }
        //Years
        else{
            if($years==1){
                return "one year ago";
            }else{
                return "$years years ago";
            }
        }
    }



}


 ?>
