

function submit_tech(_this){
	$(".services").prop("disabled", false);
	$("body").loading();
	ajax_POST_HAS_FILE('pages/create_tech',new FormData(_this))
  	return false;
}

 
$(document).ready(function(){
	$('#btn-reset').hide()
	$('#service_type').on('select2:opening', function (e) {
		
		var _this = $(this).val()
		var expertise =  $('#expertise').val()
		if(expertise == ""){
			alert('Please Select Technician Services Expertise')
		}
		else{
			$('#btn-reset').show()
			$(".services").prop("disabled", true);

			if(expertise.length > 1){
				if(_this == ""){
					var option = ''
					for(i=0;i<expertise.length;i++){
						var expertise_selected  =   expertise[i]
						var expertise_split     =   expertise_selected.split('|')
						var data                =   data_GET('pages/get_service_type/0'+'/'+6+'/'+expertise_split[0]+'/'+0)
						option              +=   '<optgroup label="'+expertise_split[1]+'">'
						$.each(data,function(x,y){
							option  +=  '<option value="'+expertise_split[0]+'|'+y.services_type_id+'">'+expertise_split[1]+ " " +y.services_type_desc+'</option>'
						})
						option +=   '</optgroup>'
						
					}
					$('#service_type').html(option)
				}
			}
			else{
				if(_this == ""){
					var expertise_selected  =   expertise[0]
					var expertise_split     =   expertise_selected.split('|')
					var data                =   data_GET('pages/get_service_type/0'+'/'+6+'/'+expertise_split[0]+'/'+0)
					var option              =   '<optgroup label="'+expertise_split[1]+'">'
					$.each(data,function(x,y){
						option  +=  '<option value="'+expertise_split[0]+'|'+y.services_type_id+'">'+expertise_split[1]+ " " +y.services_type_desc+'</option>'
					})
					option +=   '</optgroup>'
					$('#service_type').html(option)
				}
				
				
			}
		}
		
		


	  });

	  $('#register_form').submit(function(e){
		  e.preventDefault();
		  $(this).parsley().validate();
			if ($(this).parsley().isValid()){
				$("body").loading();
				var page = 'verify-code'
				ajax_POST('pages/register',$(this).serialize(),page)
			}
			
	  })

	  $('#verify-code').submit(function(e){
		e.preventDefault()
		$("body").loading();
		var page = 'login'
		ajax_POST('pages/verify_code',$(this).serialize(),page)
	  })

	  $('#resend_code').click(function(){
		$("body").loading();
		ajax_POST('pages/resend_code',$(this).serialize())
	  })

	  $('#forgot-pwd').submit(function(e){
		e.preventDefault()
		$("body").loading();
		ajax_POST('pages/forgot_pwd',$(this).serialize())
	  })

	  $('#change_password').submit(function(e){
		e.preventDefault()
		$(this).parsley().validate();
		if ($(this).parsley().isValid()){
			$("body").loading();
			var page = '../login'
			ajax_POST('../pages/update_pwd/0',$(this).serialize(),page)
		}
	})

	$('#contact_form').submit(function(e){
		e.preventDefault();
		$(this).parsley().validate();
		if ($(this).parsley().isValid()){
			$("body").loading();
			ajax_POST('pages/get_in_touch',$(this).serialize())
		}
	})

	  $('#btn-reset').click(function(){
        $('#btn-reset').hide()
		$(".services").prop("disabled", false);
        var service = $('#service_type').val()
        if(service.length > 0){
            for(i=0;i<service.length;i++){
                var $select = $('#service_type')
                var idToRemove = service[i]
                var x = service.indexOf(idToRemove);
                if (x >= 0) {
                    service.splice(x, service.length);
                    $select.val(service).change();
                }
            }
        }
	  })
	  
	  $('#transparent-pricing').submit(function(e){
		  e.preventDefault()
		  $(this).parsley().validate();
		   if ($(this).parsley().isValid()){
			   var services = $(this).find('select#t-service-type').val()
			   var city = $(this).find('select#t-city').val()
			   var services_text = $(this).find('select#t-service-type option:selected').text()
			   var city_text = $(this).find('select#t-city option:selected').text()
			   var services_id = 0
			   var services_type_id = 0
			   if(services.indexOf('|') == 1){
				   services_id = services.split('|')[0]
				   services_type_id = services.split('|')[1]
			   }
			$('#tp-title').html(services_text +' - '+city_text)
			var data    =   data_GET('pages/get_pricing/0/6/'+services_id+'/'+services_type_id+'/'+city)
			$('#tp-div').attr('hidden',false)
		
			if(data.pricing == ''){
				$('#display-transparent-price').html('<div class="col-md-12"> <p class="text-center text-danger">Sorry the service type in your city is not available.</p> </div>')
			}else{
				var div = ''
				var p = ''
				$.each(data.pricing,function(x,y){
					div += `<div class="col-md-4 mb-10">
								<img src="./assets/admin/images/appliances/${y.appliances_image}"   alt="appliances image" style="width:100px;height:100px">
								<p> <span>	${y.appliances_desc} </span> <br /> <span>₱ ${y.price}</span></p>
							</div>`
				})
				$('#display-transparent-price').html(div)
				if(data.desc[0].services_type_description != ''){
					if(data.desc[0].services_type_description.indexOf(';') > 0){
						let count = 1
						let split   =   data.desc[0].services_type_description.split(';')
						for(i = 0; i < split.length; i++){
							p += `<p class="ml-20 mb-0">${count++}. ${split[i]}</p>`
						}
					}
					else{
						p += `<p class="ml-20 mb-0">1. ${data.desc[0].services_type_description}</p>`;
					}
					$('#tp-desc').html(p)
				}
				

			}
			
		  }
	  })
	
})

$(document).ready(function()
{
	"use strict";

	/* 

	1. Vars and Inits

	*/

	var header = $('.header');
	var menu = $('.menu');
	var menuActive = false;

	setHeader();

	$(window).on('resize', function()
	{
		setHeader();

		setTimeout(function()
		{
			$(window).trigger('resize.px.parallax');
		}, 375);
	});

	$(document).on('scroll', function()
	{
		setHeader();
	});

	initMenu();
	initDeptSlider();
	initAccordions();

	/* 

	2. Set Header

	*/

	function setHeader()
	{
		if($(window).scrollTop() > 350)
		{
			// header.addClass('scrolled');
			$(".header_btn").removeClass('hide_element')
			$(".header_btn").addClass('show_element')
			$(".banner_btn").hide()
			$('.header_nav_container').css({'border-bottom': '2px solid #ffad36'})
			$('.header_li').removeClass('li_mr-13')
			$('.header_li').addClass('li_mr-9')

			

			// alert('test')
		}
		else
		{
			header.removeClass('scrolled');
			$('.header_nav_container').css({'border-bottom': 'none'})
			$(".header_btn").removeClass('show_element')
			$(".header_btn").addClass('hide_element')
			$(".banner_btn").show()
			$('.main_nav ul li').css({'margin-right':'13px !important'})
			$('.header_li').removeClass('li_mr-9')
			$('.header_li').addClass('li_mr-13')

		}
	}

	/* 

	3. Init Menu

	*/

	function initMenu()
	{
		if($('.hamburger').length && $('.menu').length)
		{
			var hamb = $('.hamburger');
			var close = $('.menu_close_container');

			hamb.on('click', function()
			{
				if(!menuActive)
				{
					openMenu();
				}
				else
				{
					closeMenu();
				}
			});

			close.on('click', function()
			{
				if(!menuActive)
				{
					openMenu();
				}
				else
				{
					closeMenu();
				}
			});

	
		}
	}

	function openMenu()
	{
		menu.addClass('active');
		menuActive = true;
	}

	function closeMenu()
	{
		menu.removeClass('active');
		menuActive = false;
	}

	/* 

	4. Init Dept Slider

	*/

	function initDeptSlider()
	{
		if($('.dept_slider').length)
		{
			var deptSlider = $('.dept_slider');
			deptSlider.owlCarousel(
			{
				items:4,
				autoplay:true,
				loop:true,
				nav:false,
				dots:false,
				margin:30,
				smartSpeed:1200,
				responsive:
				{
					0:{items:1},
					768:{items:2},
					992:{items:3},
					1200:{items:4}
				}
			});

			if($('.dept_slider_nav').length)
			{
				var next = $('.dept_slider_nav');
				next.on('click', function()
				{
					deptSlider.trigger('next.owl.carousel');
				});
			}
		}
	}

	/* 

	5. Init Accordions

	*/

	function initAccordions()
	{
		if($('.accordion').length)
		{
			var accs = $('.accordion');

			accs.each(function()
			{
				var acc = $(this);

				if(acc.hasClass('active'))
				{
					var panel = $(acc.next());
					var panelH = panel.prop('scrollHeight') + "px";
					
					if(panel.css('max-height') == "0px")
					{
						panel.css('max-height', panel.prop('scrollHeight') + "px");
					}
					else
					{
						panel.css('max-height', "0px");
					} 
				}

				acc.on('click', function()
				{
					if(acc.hasClass('active'))
					{
						acc.removeClass('active');
						var panel = $(acc.next());
						var panelH = panel.prop('scrollHeight') + "px";
						
						if(panel.css('max-height') == "0px")
						{
							panel.css('max-height', panel.prop('scrollHeight') + "px");
						}
						else
						{
							panel.css('max-height', "0px");
						} 
					}
					else
					{
						acc.addClass('active');
						var panel = $(acc.next());
						var panelH = panel.prop('scrollHeight') + "px";
						
						if(panel.css('max-height') == "0px")
						{
							panel.css('max-height', panel.prop('scrollHeight') + "px");
						}
						else
						{
							panel.css('max-height', "0px");
						} 
					}
				});
			});
		}
	}

});


function data_GET(url){
    var response = ajax_GET(url)
    return response.responseJSON.data;
}
function ajax_GET(url){
    var data = $.ajax({
        url:url,
        type:'GET',
        dataType: 'json',
        async: false,
        success:function(response){
            return response
		},
		error:function(err){
			console.log(err)
		}
    })
    return data
}


function ajax_POST(url,data, page = 0){
    $.ajax({
        url:url,
        type:"POST",
        data:data,
        success:function(response){
            condition(response,page)
        }
    })

}

function ajax_POST_HAS_FILE(url,data,page = 0){
    $.ajax({
        url:url,
        type:"POST",
        data:data,
        contentType:false,
        processData:false,
        cache:false,
        success:function(response){
            condition(response,page)
        }
    })
}

function condition(encode_data,page){
    var data = JSON.parse(encode_data)
    if(data.status == 200){
		$('body').loading('stop');		
        $.confirm({
            title: data.message,
            content: '',
            theme: 'modern',
            animation: 'scale',
            type: 'orange',
            icon: 'fa faay-check',
            buttons: {
                ok: function () {
					// location.reload()
					if(page == 0){
						location.reload()
					}
					else{
						window.location.href=page
					}
                }
            }
        });
    }
    else if(data.status == 409){
		$('body').loading('stop');
        $.confirm({
            title: data.message,
            content: '',
            theme: 'modern',
            animation: 'scale',
            type: 'orange',
            icon: 'fa fa-warning',
            buttons: {
                ok: function () {
                }
            }
        });
    }
    else{
		$('body').loading('stop');
        $.confirm({
            title: data.message,
            content: '',
            theme: 'modern',
            animation: 'scale',
            type: 'orange',
            icon: 'fa fa-times-circle',
            buttons: {
                ok: function () {
                }
            }
        });
    }
}