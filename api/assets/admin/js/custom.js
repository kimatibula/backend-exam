$(function(){
    $('#btn-reset').hide()
    // |====================|
    // |========CLIENT======|
    // |====================|
    $(document).on('click','.view-client',function(){
        $('#view-client').modal('show')
        var id      =   $(this).attr('uid')
        var role    =   $(this).attr('urole')
        var data    =   data_GET('admin_pages/get_one_user/'+id+'/'+role)
        $('#vc_first_name').val(data[0].first_name)
        $('#vc_last_name').val(data[0].last_name)
        $('#vc_mobile_no').val(data[0].mobile_no)
        $('#vc_email').val(data[0].username)
        $('#vc_address').val(data[0].address)
        $('#vc_brgy').val(data[0].barangay)
        $('#vc_landmark').val(data[0].landmark)
        $('#vc_zipcode').val(data[0].zipcode)
        $('#vc_province').val(data[0].province)
        $('#vc_last_name').val(data[0].last_name)
        select('#vc_city',data[0].city)
    })

    $('#save_user').submit(function(e){
        e.preventDefault()
        if (form_validate($(this))){
           ajax_POST('admin_pages/save_client',$(this).serialize(),'#add-client')
        }
        
    })

    // |========================|
    // |========TECHNICIAN======|
    // |========================|

    $(document).on('click','.view-technician',function(){
        $('#view-technician').modal('show')
        var id      =   $(this).attr('uid')
        var role    =   $(this).attr('urole')
        var data    =   data_GET('admin_pages/get_one_user/'+id+'/'+role)
        $('#vt_first_name').val(data[0].first_name)
        $('#vt_last_name').val(data[0].last_name)
        $('#vt_mobile_no').val(data[0].mobile_no)
        $('#vt_email').val(data[0].username)
        $('#vt_address').val(data[0].address)
        $('#vt_brgy').val(data[0].barangay)
        $('#vt_landmark').val(data[0].landmark)
        $('#vt_zipcode').val(data[0].zipcode)
        $('#vt_province').val(data[0].province)
        $('#vt_last_name').val(data[0].last_name)
        select('#vt_city',data[0].city)
        radio_3(data[0].is_employed,'#is_employed_yes','#is_employed_self','#is_employed_no')
        // if(data[0].is_employed == 1,){
        //     $('#is_employed_yes').attr('checked','checked')
        // }
        // else if(data[0]['is_employed'] == 2){
        //     $('#is_employed_self').attr('checked','checked')
        // }
        // else{
        //     $('#is_employed_no').attr('checked','checked')

        // }
        $('#vt_job_experience').val(data[0].experience)
        var service = data[0].services
        var services_type       =   data[0].services_type
        var certificate         =   data[0].valid_certificate
        var security_clearnce   =   data[0].valid_security_clearance
        // SERVICES
        if(service.indexOf(',') > 0){
            let service_split   =   service.split(',')
            let val             =   []
            for(i = 0; i < service_split.length; i++){
                val.push(service_split[i].split('|')[0])
            }
            // $('#vt_expertise').selectpicker('val', val)
            $('#vt_expertise').val(val).trigger("change");
        }
        else{
            // $('#vt_expertise').selectpicker('val', service.split('|')[0])
            $('#vt_expertise').val(service.split('|')[0]).trigger("change");
            
        }
        // SERVICES TYPE
        if(services_type.indexOf(',') > 0){
            let service_type   =   services_type.split(',')
            let val             =   []
            for(i = 0; i < service_type.length; i++){
                val.push(service_type[i])
            }
            console.log(val)
            $('#vt_service_type').val(val).trigger("change");
        }
        else{
            $('#vt_service_type').val(services_type).trigger("change");
        }
        // CERTIFICATE
        if(certificate.indexOf(',') > 0){
            let certificate_split   =   certificate.split(',')
            let val                 =   []
            for(x = 0; x < certificate_split.length; x++){
                val.push(certificate_split[x])
            }
            // $('#vt_valid_certificate').selectpicker('val', val)
            $('#vt_valid_certificate').val(val).trigger("change");

        }
        else{
            // $('#vt_valid_certificate').selectpicker('val', certificate)
            $('#vt_valid_certificate').val(certificate).trigger("change");
            
        }

        // SECURITY CLEARANCE
        if(security_clearnce.indexOf(',') > 0){
            let security_clearnce_split   =   security_clearnce.split(',')
            let val                 =   []
            for(x = 0; x < security_clearnce_split.length; x++){
                val.push(security_clearnce_split[x])
            }
            // $('#vt_security_clearance').selectpicker('val', val)
            $('#vt_security_clearance').val(val).trigger("change");

        }
        else{
            // $('#vt_security_clearance').selectpicker('val', security_clearnce)
            $('#vt_security_clearance').val(security_clearnce).trigger("change");

        }
        // PREFERRED SERVICES CITY
        $('#vt_preffered_service').selectpicker('val', 2)

        select('#vt_transportation_mode',data[0].transportation_mode)
        radio_3(data[0].have_job_tools,'#vt_jobtools_yes','#vt_jobtools_some','#vt_jobtools_no')
        select('#source_of_info',data[0].source_of_info)
  

        
    })

    
    $('#btn-reset').click(function(){
        $('#btn-reset').hide()
        $('#expertise').parent().find('button.dropdown-toggle').attr('disabled',false)
        var service = $('#service_type').val()
        if(service.length > 0){
            for(i=0;i<service.length;i++){
                var $select = $('#service_type')
                var idToRemove = service[i]
                var x = service.indexOf(idToRemove);
                if (x >= 0) {
                    service.splice(x, service.length);
                    $select.val(service).change();
                }
            }
        }
      })
    $("#service_type").select2().on('select2-focus', function(){
        if($(this).val() === null){
            var expertise =  $('#expertise').val()
            if(expertise.length > 0){
                $('#btn-reset').show()
                $('.bootstrap-select.open').removeClass('open');
                $('#expertise').parent().find('button.dropdown-toggle').attr('disabled',true)
            }
            if(expertise.length > 1){
                var option = ''
                for(i=0;i<expertise.length;i++){
                    var expertise_selected  =   expertise[i]
                    var expertise_split     =   expertise_selected.split('|')
                    var data                =   data_GET('admin_pages/get_pricing/0'+'/'+6+'/'+expertise_split[0]+'/'+0+'/0')
                    option              +=   '<optgroup label="'+expertise_split[1]+'">'
                    $.each(data,function(x,y){
                        option  +=  '<option value="'+expertise_split[0]+'|'+y.services_type_id+'">'+expertise_split[1]+ " " +y.services_type_desc+'</option>'
                    })
                    option +=   '</optgroup>'
                    
                }
                $('#service_type').html(option)
            }
          
            if(expertise.length === 1){
                var expertise_selected  =   expertise[0]
                var expertise_split     =   expertise_selected.split('|')
                var data                =   data_GET('admin_pages/get_pricing/0'+'/'+6+'/'+expertise_split[0]+'/'+0+'/0')
                var option              =   '<optgroup label="'+expertise_split[1]+'">'
                $.each(data,function(x,y){
                    option  +=  '<option value="'+expertise_split[0]+'|'+y.services_type_id+'">'+expertise_split[1]+ " " +y.services_type_desc+'</option>'
                })
                option +=   '</optgroup>'
                $('#service_type').html(option)
            }
        }
   });
    $('#form-add-technician').submit(function(e){
        e.preventDefault()
        $('#tech_expertise_error').removeAttr('hidden',false)
        if (form_validate($(this))){
            if(validate_other_info('#expertise','#service_type','#valid_certificate','#security_clearance','#preffered_service')){
                ajax_POST('admin_pages/save_technician',$(this).serialize(),'#add-technician') 
            }
        }
    })

    $(document).on('click','.add-security-cleance',function(e){
        $('#add-certificate-clearance').modal('show')
        $('#tech-name').html($(this).attr('tech-name'))
        $('#atid').val($(this).attr('taid'))
    })

    $('#form-add-certificate-clearance').submit(function(e){
        e.preventDefault()
        if (form_validate($(this))){
            if(validate_certificate_clearance('#add_valid_certificate','#add_security_clearance')){
                ajax_POST('admin_pages/update_certificate_clearance',$(this).serialize(),'#add-certificate-clearance') 
            }
        }
    })


    $(document).on('click','.btn-action',function(){
        var id      =   $(this).attr('uid') 
        var text    =   $(this).attr('text')
        var status  =   $(this).attr('ustatus')
        $.confirm({
            title: text+" user",
            content: 'Are you sure to '+$(this).attr('text')+ " this user?",
            icon: 'fa fa-question',
            theme: 'modern',
            closeIcon: true,
            animation: 'scale',
            type: 'orange',
            buttons: {
                confirm: function () {
                    ajax_POST('admin_pages/user_update_status/'+id,{'status':status},'')
                },
                cancel: function () {
                }
            }
        });
        
    })

    
    // |======================|
    // |========SERVICES======|
    // |======================|

    $(document).on('click','.view-service',function(){
        var id      =   $(this).attr('sid')   
        $('#view-service').modal('show')
        var data    = data_GET('admin_pages/get_service/'+id)
        $('#es_service').val(data[0].services_desc)
        $('#es_service_id').val(data[0].services_id)
        check_radio(data[0].status_id,'#es_enable','#es_disable')
        
    })

    $('#update-service').submit(function(e){
        e.preventDefault();
        if (form_validate($(this))){
            ajax_POST('admin_pages/update_service',$(this).serialize(),'#view-service')
        }
    })


    $('#save_service').submit(function(e){
        e.preventDefault()
        if (form_validate($(this))){
           ajax_POST('admin_pages/save_service',$(this).serialize(),'#add-service')
        }
    })



    // |========================|
    // |========APPLIANCES======|
    // |========================|


    $('#save_appliances').submit(function(e){
        e.preventDefault()
        if (form_validate($(this))){
            ajax_POST_HAS_FILE('admin_pages/create_appliances',new FormData(this),'#add-apliances')
        }
    })

    $(document).on('click','.view-appliances',function(){
        $('#view-appliances').modal('show')
        var id      =   $(this).attr('aid')
        var data    = data_GET('admin_pages/get_appliances/'+id)
        // select('#ea_service',data[0].services_id)
        $('#ea_appliances_name').val(data[0].appliances_name)
        $('#ea_appliances').val(data[0].appliances_desc)
        $('#display_image_view').attr('src','./assets/admin/images/appliances/'+data[0].appliances_image)
        check_radio(data[0].status_id,'#ea_enable','#ea_disable')
        $('#ea_id').val(data[0].appliances_id)

    })

    $('#edit_appliances').submit(function(e){
        e.preventDefault()
        if (form_validate($(this))){
            ajax_POST_HAS_FILE('admin_pages/update_appliances',new FormData(this),'#view-appliances')
        }
    })

    // |==========================|
    // |========SERVICE TYPE======|
    // |==========================|

    
    $('#save_services_type').submit(function(e){
        e.preventDefault()
        if (form_validate($(this))){
            ajax_POST('admin_pages/save_service_type',$(this).serialize(),'#add-service-type')
        }

    })

    $(document).on('click','.view-service-type',function(){
        $('#view-service-type').modal('show')
        var id      =   $(this).attr('stid')
        var data    =   data_GET('admin_pages/get_service_type/'+id)
        $('#est_id').val(data[0].services_type_id)
        $('#est_service_type').val(data[0].services_type_desc)
        check_radio(data[0].status_id,'#est_enable','#est_disable')
        // get_appliances_under_services(0,services,6,'#est_appliances',data[0].appliances_id) 
    })

    $('#update_services_type').submit(function(e){
        e.preventDefault()
        if (form_validate($(this))){
            ajax_POST('admin_pages/update_service_type',$(this).serialize(),'#view-service-type')
        }
    })

    // |======================================|
    // |========SERVICE TYPE DESCRIPTION======|
    // |======================================|
    $('#save_services_type_description').submit(function(e){
        e.preventDefault()
        if (form_validate($(this))){
            ajax_POST('admin_pages/create_services_desc',$(this).serialize(),'#add-service-type-desc')
        }
    })

    $(document).on('click','.view-service-type-desc',function(){
       
        $('#view-service-type-desc').modal('show')
        var id = $(this).attr('stdid')
        var data    = data_GET('admin_pages/get_services_desc/'+id)
        select('#estd_services',data[0].services_id)
        // get_services_type_under_services(data[0].services_id , 0, 0, 0, '#estd_services_type', data[0].services_type_id)
        get_services_type(data[0].services_id,'#estd_services_type', 6,data[0].services_type_id)
        $('#estd_services_type_desc').val(data[0].services_type_description)
        $('#estd_id').val(data[0].services_type_description_id)
    })

    $('#update_services_type_desc').submit(function(e){
        e.preventDefault()
        if (form_validate($(this))){
            ajax_POST('admin_pages/update_services_desc',$(this).serialize(),'#view-service-type-desc')
        }
    })
   
    $(document).on('click','.remove-service-type-desc',function(){
        var id      =   $(this).attr('stdid')
        $.confirm({
            title: "Remove Services Description",
            content: 'Are you sure to Remove Services Description?',
            icon: 'fa fa-question',
            theme: 'modern',
            closeIcon: true,
            animation: 'scale',
            type: 'orange',
            buttons: {
                confirm: function () {
                    ajax_POST('admin_pages/delete_services_desc',{'id':id},'')
                },
                cancel: function () {
                }
            }
        });
        
    })

    // |==================|
    // |========CITY======|
    // |==================|

    $('#save_city').submit(function(e){
        e.preventDefault()
        if (form_validate($(this))){
            ajax_POST('admin_pages/add_city',$(this).serialize(),'#add-city')
        }
    })

    $(document).on('click','.view-city',function(){
        $('#view-city').modal('show')
        var id      =   $(this).attr('cid')
        var data    =   data_GET('admin_pages/get_city/'+id)
        $('#ce_id').val(data[0].city_id)
        $('#ce_city').val(data[0].city_desc)
        check_radio(data[0].status_id,'#ce_enable','#ce_disable')

    })

    $('#update_city').submit(function(e){
        e.preventDefault()
        if (form_validate($(this))){
            ajax_POST('admin_pages/update_city',$(this).serialize(),'#view-city')
        }
    })

    // |======================|
    // |========PRICING=======|
    // |======================|

    $(document).on('click','.edit-pricing',function(){
        $('#edit-pricing').modal('show')
        var id          =   $(this).attr('pid')
        var data        =   data_GET('admin_pages/get_pricing/'+id)
        var services    =   data[0].services_id
        var city        =   data[0].city_id  
        console.log(data)
        // var appliances  =   data[0].appliances_id
        $('#ep_id').val(data[0].pricing_id)
        select('#ep_service',services)
        select('#ep_city',city)
        select('#ep_service_type',data[0].services_type_id)
        select('#ep_appliances',data[0].appliances_id)
        $('#ep_price').val(data[0].price)
        check_radio(data[0].status_id,'#ep_enable','#ep_disable')
        // get_appliances_under_services(0,services,6,'#ep_appliances',data[0].appliances_id)
        // services_type_under_appliances(0, 6, services, appliances, '#ep_service_type')
    })

    $('#save_pricing').submit(function(e){
        e.preventDefault()
        if (form_validate($(this))){
            ajax_POST('admin_pages/save_pricing',$(this).serialize(),'#add-pricing')        
        }
    })

    $('#update_pricing').submit(function(e){
        e.preventDefault()
        if (form_validate($(this))){
            ajax_POST('admin_pages/update_pricing',$(this).serialize(),'#edit-pricing')
        }
    })

    // |=======================|
    // |========DATATABLE======|
    // |=======================|

    $('#all_client').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": 'admin_pages/getAllUsers/3',
        "columnDefs": [ {
        "targets": 8,
        "orderable": false
        } ]
    } );

    $('#all_technician').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax": 'admin_pages/getAllUsers/2',
        "columnDefs": [ {
        "targets": 8,
        "orderable": false
        } ]
    });

    $('#all_pricing').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax": 'admin_pages/get_all_pricing',
        "columnDefs": [ {
        "targets": 8,
        "orderable": false
        } ]
    });

    $('#all_services').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax": 'admin_pages/get_all_service',
        "columnDefs": [ {
        "targets": 4,
        "orderable": false
        } ]
    });

    $('#all_city').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax": 'admin_pages/get_all_city',
        "columnDefs": [ {
        "targets": 4,
        "orderable": false
        } ]
    });
    $('#all_services_type').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax": 'admin_pages/get_all_service_type',
        "columnDefs": [ {
        "targets": 4,
        "orderable": false
        }]
    });
    $('#all_appliances').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax": 'admin_pages/get_all_appliances',
        "columnDefs": [ {
        "targets": [3,6],
        "orderable": false
        }]
    });

    $('#all_services_type_desc').dataTable({
        "processing": true,
        "serverSide": true,
        "ajax": 'admin_pages/get_all_service_type_description',
        "columnDefs": [ {
        "targets": 5,
        "orderable": false
        }]
    });
    


});

function radio_3(num,element1,element2,element3){
    if(num == 1){
        $(element1).attr('checked','checked')
    }
    else if(num == 2){
        $(element2).attr('checked','checked')
    }
    else{
        $(element3).attr('checked','checked')

    }
}

function form_validate(form){
    form.parsley().validate();
        if (form.parsley().isValid()){
            return true
        }
        else{
            return false
        }
}


// function get_appliances_under_services(appliances_id,service_id,status,element,appliances_equal = 0,is_display_services = 0, element_display_services = ''){
//     var service = 0;
//     if(typeof service_id === 'object'){
//         service = service_id.value
//     }
//     else{
//         service = service_id
//     }
//     var data    = data_GET('admin_pages/get_appliances/'+appliances_id+'/'+status+'/'+service)
//     var option = ''
//     if(appliances_equal == 0){
//         $.each(data,function(x,y){
//             option += '<option value="" hidden>Select appliances</option><option value="'+y.appliances_id+'">'+y.appliances_desc+'</option>'
//         })
//     }
//     else{
//         $.each(data,function(x,y){
//             var selected = (y.appliances_id == appliances_equal) ? 'selected' : ''
//             option += '<option value="'+y.appliances_id+'" '+selected+'>'+y.appliances_desc+'</option>'
//         })
//     }
//     $(element).html(option)

//     if(is_display_services != 0){
//         services_type_under_appliances(0, 6, service, data[0].appliances_id, element_display_services)
//     }
// }

// function services_type_under_appliances(services_type_id, status, service_id, appliances_id, element, service_type_equal = 0){
//     var service = 0;
//     var appliances = 0;
//     if(service_id === '#service' || service_id === '#ep_service'){
//         service = $(service_id).val()
//     }
//     else{
//         service = service_id
//     }
//     if(appliances_id != 0){
        
//     }
//     if(typeof appliances_id === 'object'){
//         appliances  =   appliances_id.value
//     }
//     else{
//         appliances  =   appliances_id
//     }
//     var data    = data_GET('admin_pages/get_service_type/'+services_type_id+'/'+status+'/'+service+'/'+appliances)
    
//     var option = ''
//     if(service_type_equal == 0){
//         $.each(data,function(x,y){
//             option += '<option value="'+y.services_type_id+'">'+y.services_type_desc+'</option>'
//         })
//     }
//     else{
//         $.each(data,function(x,y){
//             var selected = (y.services_type_id == appliances_equal) ? 'selected' : ''
//             option += '<option value="'+y.services_type_id+'" '+selected+'>'+y.services_type_desc+'</option>'
//         })
//     }
//     $(element).html(option)
// }

function get_services_type(services_id = 0,element, status = 0,service_type_equal = 0 ){
    var services = 0
    if(typeof services_id === 'object'){
        services  =   services_id.value
    }
    else{
        services  =   services_id
    }
    var data    = data_GET('admin_pages/get_pricing/0/'+status+'/'+services)
   
    var option = ''
    if(service_type_equal == 0){
        $.each(data,function(x,y){
            option += '<option value="'+y.services_type_id+'">'+y.services_type_desc+'</option>'
        })
    }
    else{
        $.each(data,function(x,y){
            var selected = (y.services_type_id == service_type_equal) ? 'selected' : ''
            option += '<option value="'+y.services_type_id+'" '+selected+'>'+y.services_type_desc+'</option>'
        })
    }
    $(element).html(option)
}

function check_radio(status,enable,disable){
    if(status == 6){
        $(enable).attr('checked','checked')
    }
    else{
        $(disable).attr('checked','checked')
    }
}

function select(element,is_equal){
    $('select'+element+' option').each(function(){
        if($(this).val() == is_equal){
            $(this).attr("selected","selected");    
        }
    }); 
}

function validate_radio(name){
    var radios = $(name)
    var formValid = false

    var i = 0;
    while (!formValid && i < radios.length) {
        if (radios[i].checked) formValid = true
        i++;        
    }

    if (!formValid){
        return false
    }else{
        return true
    }
}



function readURL(input,element) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(element)
                .attr('src', e.target.result)
                .width(100)
                .height(100);
        };

        reader.readAsDataURL(input.files[0]);
    }
}
function data_GET(url){
    var response = ajax_GET(url)
    return response.responseJSON.data;
}
function ajax_GET(url){
    var data = $.ajax({
        url:url,
        type:'GET',
        dataType: 'json',
        async: false,
        success:function(response){
            return response
        },
        error:function(err){
            console.log(err)
        }
    })
    return data
}


function ajax_POST(url,data,modal){
    $.ajax({
        url:url,
        type:"POST",
        data:data,
        success:function(response){
            condition(response,modal)
        }
    })

}

function ajax_POST_HAS_FILE(url,data,modal){
    $.ajax({
        url:url,
        type:"POST",
        data:data,
        contentType:false,
        processData:false,
        cache:false,
        success:function(response){
            condition(response,modal)
        }
    })
}

function condition(encode_data, modal_id){
    var data = JSON.parse(encode_data)
    if(data.status == 200){
        $(modal_id).modal('hide')
        $.confirm({
            title: data.message,
            content: '',
            theme: 'modern',
            animation: 'scale',
            type: 'orange',
            icon: 'fa fa-check',
            buttons: {
                ok: function () {
                    location.reload()
                }
            }
        });
    }
    else if(data.status == 409){
        $.confirm({
            title: data.message,
            content: '',
            theme: 'modern',
            animation: 'scale',
            type: 'orange',
            icon: 'fa fa-warning',
            buttons: {
                ok: function () {
                }
            }
        });
    }
    else{
        $.confirm({
            title: data.message,
            content: '',
            theme: 'modern',
            animation: 'scale',
            type: 'orange',
            icon: 'fa fa-times-circle',
            buttons: {
                ok: function () {
                }
            }
        });
    }
}

function validate_certificate_clearance(certificate,clearance){
    var flag = 0
    var valid_crtfct    =   $(certificate).val()
    var security_clrnce =   $(clearance).val()
    if(valid_crtfct === null){
        $(certificate+'_error').attr('hidden',false)
        $(certificate).parent().find('.bootstrap-select').css({'border':'1px solid red'})
        flag    =   1

        
    }
    else{
        $(certificate+'_error').attr('hidden',true)
        $(certificate).parent().find('.bootstrap-select').css({'border':'none'})
         
    }
    if(security_clrnce === null){
        $(clearance+'_error').attr('hidden',false)
        $(clearance).parent().find('.bootstrap-select').css({'border':'1px solid red'})
        flag    =   1
        
    }
    else{
        $(clearance+'_error').attr('hidden',true)
        $(clearance).parent().find('.bootstrap-select').css({'border':'none'})
    }
    if(flag > 0){
        return false
    }
    else{
        return true
    }
}

function validate_other_info(expertise,service_type,valid_certificate,clearance,prefferred_service){
    var flag            =   0
    var exp             =   $(expertise).val()
    var st              =   $(service_type).val()
    var valid_crtfct    =   $(valid_certificate).val()
    var security_clrnce =   $(clearance).val()
    var preferred_service=  $(prefferred_service).val()
    if(exp === null){
        $(expertise+'_error').attr('hidden',false)
        $(expertise).parent().find('.bootstrap-select').css({'border':'1px solid red'})
        flag    =   1
    }
    else{
        $(expertise+'_error').attr('hidden',true)
        $(expertise).parent().find('.bootstrap-select').css({'border':'none'})

    }
    if(st === null){
        $(service_type+'_error').attr('hidden',false)
        $(service_type).parent().find('#s2id_service_type').css({'border':'1px solid red'})
        flag    =   1
        
    }
    else{
        $(service_type+'_error').attr('hidden',true)
        $(service_type).parent().find('#s2id_service_type').css({'border':'none'})
    }

    if(valid_crtfct === null){
        $(valid_certificate+'_error').attr('hidden',false)
        $(valid_certificate).parent().find('.bootstrap-select').css({'border':'1px solid red'})
        flag    =   1

        
    }
    else{
        $(valid_certificate+'_error').attr('hidden',true)
        $(valid_certificate).parent().find('.bootstrap-select').css({'border':'none'})
         
    }
    if(security_clrnce === null){
        $(clearance+'_error').attr('hidden',false)
        $(clearance).parent().find('.bootstrap-select').css({'border':'1px solid red'})
        flag    =   1
        
    }
    else{
        $(clearance+'_error').attr('hidden',true)
        $(clearance).parent().find('.bootstrap-select').css({'border':'none'})
    }
    if(preferred_service === ''){
        $(prefferred_service+'_error').attr('hidden',false)
        $(prefferred_service).parent().find('.bootstrap-select').css({'border':'1px solid red'})
        flag    =   1
        
    }
    else{
        $(prefferred_service+'_error').attr('hidden',true)
        $(prefferred_service).parent().find('.bootstrap-select').css({'border':'none'})
    }
    if(flag > 0){
        return false
    }
    else{
        return true
    }
}